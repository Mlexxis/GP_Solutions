package task278;

import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);
        String start = "";
        String end = "";
        while (in.hasNext()) {
            start = in.nextLine();
            end = in.nextLine();
        }
        in.close();
        if (start.length() > end.length()) {
            out.println("NO");
            out.flush();
            return;
        }
        int restartPosition = 0;
        for (int i = 0; i < start.length(); i++) {
            for (int j = restartPosition; j < end.length(); j++) {
                if (start.charAt(i) == end.charAt(j)) {
                    restartPosition = j+1;
                    break;
                }
                if ((j == (end.length()-1)) && ((i != (start.length()-1)))
                   ||((j == (end.length()-1)) && ((i == (start.length()-1))) && (start.charAt(i) != end.charAt(j)))) {
                    out.println("NO");
                    out.flush();
                    return;
                }
            }
        }
        out.println("YES");
        out.flush();
    }
}
