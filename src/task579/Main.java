package task579;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Scanner in = new Scanner(new File("input.txt"));
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);
        in.nextInt();
        int i = 1;
        int positiveSum = 0;
        int negativeSum = 0;
        int positiveLength = 0;
        int negativeLength = 0;
        StringBuilder positiveIndecies = new StringBuilder();
        StringBuilder negativeIndecies = new StringBuilder();
        while (in.hasNext()) {
            int tmp = in.nextInt();
            if (tmp > 0) {
                positiveSum +=tmp;
                positiveLength++;
                positiveIndecies.append(i).append(" ");
            }
            else {
                negativeSum +=tmp;
                negativeLength++;
                negativeIndecies.append(i).append(" ");
            }
            i++;
        }
        in.close();
        if (positiveSum > Math.abs(negativeSum)) {
            out.println(positiveLength);
            out.println(positiveIndecies.toString().trim());
        } else {
            out.println(negativeLength);
            out.println(negativeIndecies.toString().trim());
        }
        out.flush();
    }

}
