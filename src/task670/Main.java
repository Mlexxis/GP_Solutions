package task670;

import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);
        int iterator = in.nextInt();
        in.close();
        for (int i = 0; i <= iterator; i++) {
            int tmp = (int) String.valueOf(i).chars().distinct().count();
            if ((String.valueOf(i)).length() != tmp) {
                iterator++;
            }
        }
        out.println(iterator);
        out.flush();
    }
}
