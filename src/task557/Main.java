package task557;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private int matrixNumber;
    private int matrixSize;
    private int filter;
    private int searchRow;
    private int searchColumn;
    List<Integer> list;
    int iterator = 0;

    public static void main(String[] args)  {
        new Main().start();
    }

    void start () {

        try {
            readInput();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (matrixSize > 30)
        largeMatrix(list);
        else smallNatrix(list);

    }

    void largeMatrix (List <Integer> list) {
        int [][] result = createUnitMatrix();

        result = multiplyFast(result, createMatrix());
        while (iterator < list.size()) {
            result = multiplyFast(result, createMatrix());
        }
        PrintWriter out = new PrintWriter(System.out);
        out.println(result[searchRow-1][searchColumn-1]);
        out.close();
    }

    void smallNatrix (List <Integer> list) {
        int [][] finalMatrix = new int[matrixSize][matrixSize];
        for (int i = 0; i < finalMatrix.length; i++) {
            for (int j = 0; j < finalMatrix[i].length; j++) {
                if (i == j)
                    finalMatrix[i][j] = 1;
            }
        }
        for (int j = 0; j < list.size(); j++) {
            int [][] tmp = new int[matrixSize][matrixSize];
            int i = 0;
            int t = 0;
            for (; i < matrixSize*matrixSize;) {
                for (int s = 0; s < tmp.length; s++) {
                    for (int k = 0; k < matrixSize; k++) {
                        tmp[k][s] += list.get(j + i)*finalMatrix[k][t];
                        if (tmp[k][s] > filter)
                            tmp[k][s] %= filter;
                    }
                    i++;
                    if (i%(matrixSize) == 0)
                        t ++;
                }
            }
            j += i-1;
            finalMatrix = tmp;
        }
        PrintWriter out = new PrintWriter(System.out);
        out.println(finalMatrix[searchRow-1][searchColumn-1]);
        out.close();
    }


    int [][] multiplyFast (int [][] matrixA, int [][] matrixB) {
        int [][][] blockedA = splitMatrix(matrixA);
        int [][][] blockedB = splitMatrix(matrixB);
        int [][] mS01 = sum(blockedA[2], blockedA[3]);
        int [][] mS02 = subtract(mS01, blockedA[0]);
        int [][] mS03 = subtract(blockedA[0], blockedA[2]);
        int [][] mS04 = subtract(blockedA[1], mS02);
        int [][] mS05 = subtract(blockedB[1], blockedB[0]);
        int [][] mS06 = subtract(blockedB[3], mS05);
        int [][] mS07 = subtract(blockedB[3], blockedB[1]);
        int [][] mS08 = subtract(mS06, blockedB[2]);
        int [][] mP01 = multiply(mS02,  mS06);
        int [][] mP02 = multiply(blockedA[0], blockedB[0]);
        int [][] mP03 = multiply(blockedA[1], blockedB[2]);
        int [][] mP04 = multiply(mS03, mS07);
        int [][] mP05 = multiply(mS01, mS05);
        int [][] mP06 = multiply(mS04, blockedB[3]);
        int [][] mP07 = multiply(blockedA[3], mS08);
        int [][] mC11 = sum(mP02, mP03);
        int [][] mC12 = sum(sum(sum(mP01, mP02), mP05), mP06);
        int [][] mC21 = subtract(sum(sum(mP01, mP02), mP04), mP07);
        int [][] mC22 = sum(sum(sum(mP01, mP02), mP04), mP05);

        return combineMatrices(mC11, mC12, mC21, mC22);
    }

    int [][] multiply (int [][] matrixA, int [][] matrixB) {
        int [][] result = new int [matrixA.length][matrixA.length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                for (int l = 0; l < matrixA.length; l++) {
                   result[i][j] += matrixA[i][l]*matrixB[l][j];
                   if (result[i][j] >= filter)
                       result[i][j] = result[i][j]%filter;
                }
            }
        }
        return result;
    }

    void readInput () throws FileNotFoundException {
        //Scanner in = new Scanner(new File("input.txt"));
        Scanner in = new Scanner(System.in);
        matrixNumber = in.nextInt();
        matrixSize = in.nextInt();
        searchRow = in.nextInt();
        searchColumn = in.nextInt();
        filter = in.nextInt();
        list = new ArrayList<>();
        while (in.hasNext()) {
            list.add(in.nextInt());
        }
    }

    int [][] createMatrix () {
            int [][] matrix;
        if (matrixSize%2 == 0)
                matrix = new int[matrixSize][matrixSize];
        else matrix = new int[matrixSize+1][matrixSize+1];

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                matrix[i][j] = list.get(iterator);
                iterator++;
            }
        }
        return matrix;
    }

    int [][] createUnitMatrix () {
        int [][] unitMatrix;
        if (matrixSize%2 == 0)
            unitMatrix = new int[matrixSize][matrixSize];
        else unitMatrix = new int[matrixSize+1][matrixSize+1];




        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                if (i == j)
                unitMatrix[i][j] = 1;
            }
        }
        return unitMatrix;
    }

    int [][][] splitMatrix (int [][] matrix) {
        int length = matrix.length/2;
        int [][][] blockedMatrix = new int[4][length][length];
        for (int j = 0; j < length; j++) {
            for (int k = 0; k < length; k++) {
                blockedMatrix[0][j][k] = matrix[j][k];
            }
            for (int k = 0; k < length; k++) {
                blockedMatrix[1][j][k] = matrix[j][k+length];
            }
        }
        for (int j = 0; j < length; j++) {
            for (int k = 0; k < length; k++) {
                blockedMatrix[2][j][k] = matrix[j+length][k];
            }
            for (int k = 0; k < length; k++) {
                blockedMatrix[3][j][k] = matrix[j+length][k+length];
            }
        }
        return blockedMatrix;
    }

    int [][] sum (int [][] matrixA, int [][] matrixB) {
        int [][] result = new int[matrixA.length][matrixA.length];
        for (int i = 0; i < matrixA.length; i++) {
                for (int k = 0; k < matrixA[i].length; k++) {
                    result[i][k] = matrixA[i][k] + matrixB[i][k];
                    if (result[i][k] >= filter)
                        result[i][k] = result[i][k] % filter;
                }
            }
        return result;
    }

    int [][] subtract (int [][] matrixA, int [][] matrixB) {
        int [][] result = new int[matrixA.length][matrixA.length];
        for (int i = 0; i < matrixA.length; i++) {
            for (int k = 0; k < matrixA[i].length; k++) {
                result[i][k] = matrixA[i][k] - matrixB[i][k];
                if (result[i][k] >= filter)
                    result[i][k] = result[i][k]%filter;
            }
        }
        return result;
    }

    void showMatrix (int [][][] matrix) {
        for (int j = 0; j < matrix[0].length; j++) {
            for (int k = 0; k < matrix[0][j].length; k++) {
                System.out.print(matrix[0][j][k] + " ");
            }
            System.out.print("  ");
            for (int k = 0; k < matrix[1][j].length; k++) {
                System.out.print(matrix[1][j][k] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (int j = 0; j < matrix[2].length; j++) {
            for (int k = 0; k < matrix[2][j].length; k++) {
                System.out.print(matrix[2][j][k] + " ");
            }
            System.out.print("  ");
            for (int k = 0; k < matrix[3][j].length; k++) {
                System.out.print(matrix[3][j][k] + " ");
            }
            System.out.println();
        }
    }

    void showMatrix (int [][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    int [][] combineMatrices (int [][] mC11, int [][] mC12, int [][] mC21, int [][] mC22) {
        int size = mC11.length;
        int [][] result = new int[size*2][size*2];
        for (int j = 0; j < size; j++) {
            for (int k = 0; k < size; k++) {
                result[j][k] = mC11[j][k];
            }
            for (int k = 0; k < size; k++) {
                result[j][k+size] = mC12[j][k];
            }
        }
        for (int j = 0; j < size; j++) {
            for (int k = 0; k < size; k++) {
                result[j+size][k] = mC21[j][k];
            }
            for (int k = 0; k < size; k++) {
                result[j+size][k+size] = mC22[j][k];
            }
        }
        return result;
    }




}
